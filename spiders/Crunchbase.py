import scrapy 

class CrunchbaseSpider(scrapy.Spider):
    name = "crunchbase"
    allowed_domains = ["www.crunchbase.com"]
    start_urls = ["https://www.crunchbase.com/organization/openai"]


    custom_settings = {
        'FEEDS': {
            'output.json': {
                'format': 'json',
                'encoding': 'utf8',
                'overwrite': True,
            },
        }
    }

    def start_requests(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'
        }
        for url in self.start_urls:
            yield scrapy.Request(url, headers=headers, callback=self.parse)

    def parse(self, response):
        location_element = response.xpath("//span[@class='component--field-formatter field-type-identifier-multi']")
        location_text = location_element.xpath(".//a/text()").getall()
        location = ' '.join(location_text).strip()
        num_employees = response.xpath("//a[@class='component--field-formatter field-type-enum link-accent ng-star-inserted']/text()").get().strip()
        description = response.xpath("//span[@class='description']/text()").get().strip()
        funding_type = response.xpath("//a[@class='component--field-formatter field-type-enum link-accent ng-star-inserted']/text()")[1].get().strip()
        rank_org_company = response.xpath("//a[@class='component--field-formatter field-type-integer link-accent ng-star-inserted']/text()").get().strip()

        item = {
            'Location': location,
            'Number Employees': num_employees,
            'Description': description,
            'Funding Type': funding_type,
            'Rank Org Company': rank_org_company
        }

        yield item


        yield scrapy.Request(
            url='https://www.crunchbase.com/organization/openai/signals_and_news/timeline',
            callback=self.parse_news,
            headers=response.request.headers,
            cb_kwargs=dict(item=item)  # Pass the scraped item as a keyword argument
        )

    def parse_news(self, response, item):
        news_data = response.xpath("//div[@class='activity-row ng-star-inserted']")

        for index, news in enumerate(news_data):
            date_element = response.xpath(".//span[@class='component--field-formatter field-type-date ng-star-inserted']")[index]
            website_name_element = response.xpath(".//span/span[@class='ng-star-inserted']")[index]
            news_title_element = response.xpath(".//span[@class='ng-star-inserted']/a")[index]
            website_url_element = response.xpath(".//span[@class='ng-star-inserted']/a")[index]

            date = date_element.xpath(".//text()").get().strip()
            website_name = website_name_element.xpath(".//text()").get().strip()
            news_title = news_title_element.xpath(".//text()").get().strip()
            website_url = website_url_element.attrib['href']

            yield {
                'Date': date,
                'Website Name': website_name,
                'News Title': news_title,
                'Website URL': website_url
            }
